#pragma once

class TypeFind {
public:
	static LRESULT Handler(NMHDR* hdr, int subItemFrom = 0, int subItemCnt = 1) {
		NMLVFINDITEM * info = reinterpret_cast<NMLVFINDITEM*>(hdr);
		const HWND wnd = hdr->hwndFrom;
		if (info->lvfi.flags & LVFI_NEARESTXY) return -1;
		const size_t count = _ItemCount(wnd);
		if (count == 0) return -1;
		const size_t base = (size_t)info->iStart % count;
		for (size_t walk = 0; walk < count; ++walk) {
			const size_t index = (walk + base) % count;
			for (int subItem = subItemFrom; subItem < subItemFrom + subItemCnt; ++subItem) {
				if (StringPrefixMatch(info->lvfi.psz, _ItemText(wnd, index, subItem))) return (LRESULT)index;
			}
		}
		for (size_t walk = 0; walk < count; ++walk) {
			const size_t index = (walk + base) % count;
			for (int subItem = subItemFrom; subItem < subItemFrom + subItemCnt; ++subItem) {
				if (StringPartialMatch(info->lvfi.psz, _ItemText(wnd, index, subItem))) return (LRESULT)index;
			}
		}
		return -1;
	}

	static wchar_t myCharLower(wchar_t c) {
		return (wchar_t)CharLower((wchar_t*)c);
	}
	static bool StringPrefixMatch(const wchar_t * part, const wchar_t * str) {
		unsigned walk = 0;
		for (;;) {
			wchar_t c1 = part[walk], c2 = str[walk];
			if (c1 == 0) return true;
			if (c2 == 0) return false;
			if (myCharLower(c1) != myCharLower(c2)) return false;
			++walk;
		}
	}

	static bool StringPartialMatch(const wchar_t * part, const wchar_t * str) {
		unsigned base = 0;
		for (;;) {
			unsigned walk = 0;
			for (;;) {
				wchar_t c1 = part[walk], c2 = str[base + walk];
				if (c1 == 0) return true;
				if (c2 == 0) return false;
				if (myCharLower(c1) != myCharLower(c2)) break;
				++walk;
			}
			++base;
		}
	}

	static size_t _ItemCount(HWND wnd) {
		return ListView_GetItemCount(wnd);
	}
	static const wchar_t * _ItemText(HWND wnd, size_t index, int subItem = 0) {
		NMLVDISPINFO info = {};
		info.hdr.code = LVN_GETDISPINFO;
		info.hdr.idFrom = GetDlgCtrlID(wnd);
		info.hdr.hwndFrom = wnd;
		info.item.iItem = index;
		info.item.iSubItem = subItem;
		info.item.mask = LVIF_TEXT;
		::SendMessage(::GetParent(wnd), WM_NOTIFY, info.hdr.idFrom, reinterpret_cast<LPARAM>(&info));
		if (info.item.pszText == NULL) return L"";
		return info.item.pszText;
	}

};
