#include "StdAfx.h"
#include "readers.h"
#include "fullFileBuffer.h"

t_size reader_membuffer_base::read(void * p_buffer, t_size p_bytes, abort_callback & p_abort) {
	p_abort.check_e();
	t_size max = get_buffer_size();
	if (max < m_offset) uBugCheck();
	max -= m_offset;
	t_size delta = p_bytes;
	if (delta > max) delta = max;
	memcpy(p_buffer, (char*)get_buffer() + m_offset, delta);
	m_offset += delta;
	return delta;
}

void reader_membuffer_base::seek(t_filesize position, abort_callback & p_abort) {
	p_abort.check_e();
	t_filesize max = get_buffer_size();
	if (position == filesize_invalid || position > max) throw exception_io_seek_out_of_range();
	m_offset = (t_size)position;
}




file::ptr fullFileBuffer::open(const char * path, abort_callback & abort, file::ptr hint, t_filesize sizeMax) {
	//mutexScope scope(hMutex, abort);

	file::ptr f;
	if (hint.is_valid()) f = hint;
	else filesystem::g_open_read(f, path, abort);
	t_filesize fs = f->get_size(abort);
	if (fs < sizeMax) /*rejects size-unknown too*/ {
		try {
			service_ptr_t<reader_bigmem_mirror> r = new service_impl_t<reader_bigmem_mirror>();
			r->init(f, abort);
			f = r;
		}
		catch (std::bad_alloc) {}
	}
	return f;
}
