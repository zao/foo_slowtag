#include "../foobar2000/SDK/foobar2000.h"
#include <ctime>
#include <iomanip>
#include <sstream>

// {7687CBAF-707A-48DA-AD69-4967C7643EB8}
static const GUID group_parent_guid = 
{ 0x7687cbaf, 0x707a, 0x48da, { 0xad, 0x69, 0x49, 0x67, 0xc7, 0x64, 0x3e, 0xb8 } };

struct ToggleFilter : file_info_filter {
	ToggleFilter() {
		auto t = std::time(nullptr);
		auto tm = *std::localtime(&t);
		std::ostringstream oss;
		oss << std::put_time(&tm, "%F %T");
		value = oss.str();
	}

	bool apply_filter(metadb_handle_ptr location, t_filestats stats, file_info& info) override {
		if (info.meta_exists("mood")) {
			info.meta_remove_field("mood");
		}
		else {
			info.meta_add("mood", value.c_str());
		}
		return true;
	}

private:
	std::string value;
};

// {B3E218FA-D68D-4627-B90C-45EE53B788F0}
static GUID const g_slowtag_branch = 
{ 0xb3e218fa, 0xd68d, 0x4627, { 0xb9, 0xc, 0x45, 0xee, 0x53, 0xb7, 0x88, 0xf0 } };

// {F8C8BE44-7ED5-4194-9D47-2B606DC032F6}
static GUID const g_bulk_warning_threshold =
{ 0xf8c8be44, 0x7ed5, 0x4194, { 0x9d, 0x47, 0x2b, 0x60, 0x6d, 0xc0, 0x32, 0xf6 } };

static advconfig_branch_factory ac_slowtag_branch("Slow Tagger", g_slowtag_branch, advconfig_entry::guid_branch_tools, 0.0);
static advconfig_integer_factory ac_bulk_warning_threshold("Warn if applying to at least these many tracks (inactivate with zero)", g_bulk_warning_threshold, g_slowtag_branch, 0.0, 0, 0, ~0ull);

struct MoodMenu : contextmenu_item_simple {
	unsigned int get_num_items() override {
		return 1;
	}

	GUID get_parent() override {
		return group_parent_guid;
	}

	GUID get_item_guid(unsigned int idx) override {
		// {FD6C5796-D4A6-4A47-9CC8-F58C6A433BF4}
		static const GUID toggle_love_guid = 
		{ 0xfd6c5796, 0xd4a6, 0x4a47, { 0x9c, 0xc8, 0xf5, 0x8c, 0x6a, 0x43, 0x3b, 0xf4 } };

		return toggle_love_guid;
	}

	void get_item_name(unsigned int idx, pfc::string_base& out) override {
		out = "Toggle love tag";
	}

	bool get_item_description(unsigned int idx, pfc::string_base& out) override {
		return false;
	}

	void context_command(unsigned int idx, metadb_handle_list_cref handles, GUID const& caller) {
		auto softLimit = ac_bulk_warning_threshold.get();
		if (softLimit > 0 && handles.get_count() >= softLimit) {
			std::ostringstream oss;
			oss << "Attempting to flip the mood at least " << softLimit << " tracks, exceeding the configured limit in Advanced Preferences, continue?";
			enum { Ok = 1, Cancel = 2 };
			auto res = uMessageBox(nullptr, oss.str().c_str(), "Mood flip?", MB_OKCANCEL);
			if (res == Cancel) {
				return;
			}
		}
		static_api_ptr_t<metadb_io_v2> io;
		service_ptr_t<ToggleFilter> filter(new service_impl_t<ToggleFilter>());
		io->update_info_async(handles, filter, nullptr, metadb_io_v2::op_flag_delay_ui, nullptr);
	}
};

DECLARE_COMPONENT_VERSION("Slow Tagger", "1.3", "zao")

static contextmenu_group_popup_factory g_group_factory(group_parent_guid, contextmenu_groups::root, "Slow Tagger");
static contextmenu_item_factory_t<MoodMenu> g_item_factory;